#include <iostream>
#include <math.h> 
#include "opencv2/core.hpp"
#include <opencv2/opencv.hpp>
using namespace std;
using namespace cv;
Mat img, normalizeImg;
Point center1(90, 90);
void generatePoint();
bool isInteger(float num);
void preStep1();
int main() {
	img = Mat::zeros(Size(360, 360), CV_8UC3);
	Mat normalizeImg = img.clone();
	generatePoint();
	vector<Point> pointStack,excludePoint;
	Point nearestPt, nearestPt2; // aka Point B,C
	float minMag1 = numeric_limits<float>::max() ,minMag2 = numeric_limits<float>::max();
	//pre-1 normalize point
	//preStep1();

	//step1
	for (int y = 0; y < img.rows; y++) {
		for (int x = 0; x < img.cols; x++) {
			Scalar color = img.at<Vec3b>(Point(x, y));
			if (color[0] == 255 && color[1] == 255 && color[2] == 255) {
				//build stack
				pointStack.push_back(Point(x, y));
			}
		}
	}
	array<Point, 3> selectedPoint;
	while (!pointStack.empty()) {
		//step 2 : random pick 3 
		random_shuffle(pointStack.begin(), pointStack.end());
		for (int i = 0; i < 3;) {
			Point trySelected = pointStack.at(i);
			if (!excludePoint.empty()) {
				auto it = find(excludePoint.begin(), excludePoint.end(), trySelected);
				if (it == excludePoint.end()) {//element is not found
					selectedPoint[i] = trySelected;
					i++;
				}
			}
			else {
				selectedPoint[i] = trySelected;
				i++;
			}
		}
		//step 3 : find center
		Point midPoint1 = Point((selectedPoint[0].x + selectedPoint[1].x) / 2, (selectedPoint[0].y + selectedPoint[1].y) / 2);
		Point midPoint2 = Point((selectedPoint[0].x + selectedPoint[2].x) / 2, (selectedPoint[0].y + selectedPoint[2].y) / 2);
		double upper = selectedPoint[0].y - selectedPoint[1].y;
		double lower = selectedPoint[0].x - selectedPoint[1].x;
		double slope1 = upper / lower;
		upper = selectedPoint[0].y - selectedPoint[2].y;
		lower = selectedPoint[0].x - selectedPoint[2].x;
		double slope2 = upper / lower;
		//change to perpendicular slope
		slope1 = -1 / slope1;
		slope2 = -1 / slope2;
		double constant1 = -(slope1 * midPoint1.x) + midPoint1.y;
		double constant2 = -(slope2 * midPoint2.x) + midPoint2.y;
		Point center;
		center.x = (constant2 - constant1) / (slope1 - slope2);
		if (isInteger(center.x))
			center.x = ceil(center.x);
		else
			continue;
		upper = (slope2 * constant1) - (slope1 * constant2);
		lower = slope2 - slope1;
		center.y = upper / lower;
		if (isInteger(center.y))
			center.y = ceil(center.y);
		else
			continue;
		//step 4 : find radius and check error
		double radiusCenter = sqrtf((center.x - selectedPoint[0].x)*(center.x - selectedPoint[0].x) + (center.y - selectedPoint[0].y)*(center.y - selectedPoint[0].y));
		array<float, 3> testRad;
		if (center.x == 90 && center.y == 90) {
			cout << "true center [90,90]" << endl;
			cout << "tested center " << center << endl << endl;
			cout << "true radius: 60" << endl;
			cout << "tested radius center " << radiusCenter << endl << endl;
			
			for (int i = 0; i < selectedPoint.size(); i++) {
				testRad[i] = sqrtf((center.x - selectedPoint[i].x)*(center.x - selectedPoint[i].x) + (center.y - selectedPoint[i].y)*(center.y - selectedPoint[i].y));
				cout << "testRadius " << testRad.at(i) << endl;
				if (radiusCenter != testRad[i]) {
					//excludePoint.push_back(selectedPoint[i]);
					cout << "Not match!" << endl<<endl;
					break;
				}
			}
		}
	}
	imshow("img", img);
	waitKey(0);
	return 0;
}
bool isInteger(float num) {
	if (floor(num) == num)
	{
		return true;
	}
	return false;
}
void generatePoint() {
	srand(time(NULL));
	Point center1(90, 90);
	Point center2(180, 90);
	Point center3(135, 135);
	int radius = 60;
	for (int i = 0; i < 200; i++) {
		int random = rand() % 360;
		int angle = random;
		int x = center1.x + cos(angle) *radius;
		int y = center1.y + sin(angle) *radius;
		Point plotPoint(x, y);
		Vec3b color(255, 255, 255);
		img.at<Vec3b>(plotPoint) = color;
	}
	/*for (int i = 0; i < 10; i++) {
		int random = rand() % 360;
		int angle = random;
		int x = center2.x + cos(angle) *radius;
		int y = center2.y + sin(angle) *radius;
		Point plotPoint(x, y);
		Vec3b color(255, 255, 255);
		img.at<Vec3b>(plotPoint) = color;
	}*/
	/*for (int i = 0; i < 50; i++) {
		int random = rand() % 360;
		int angle = random;
		int x = center3.x + cos(angle) *radius;
		int y = center3.y + sin(angle) *radius;
		Point plotPoint(x, y);
		Vec3b color(255, 255, 255);
		img.at<Vec3b>(plotPoint) = color;
	}*/
}
void preStep1() {
	vector<Point> pointNormalize;
	for (int y = 0; y < img.rows; y++) {
		for (int x = 0; x < img.cols; x++) {
			Scalar color = img.at<Vec3b>(Point(x, y));
			if (color[0] == 255 && color[1] == 255 && color[2] == 255) {
				//build stack
				pointNormalize.push_back(Point(x, y));
			}
			else if (!pointNormalize.empty() && color[0] == 0 && color[1] == 0 && color[2] == 0 && pointNormalize.size() != 1) {
				auto mid = pointNormalize.begin() + pointNormalize.size() / 2;
				Point middle = *mid;
				Vec3b color(255, 255, 255);
				normalizeImg.at<Vec3b>(middle) = color;
				pointNormalize.clear();
			}
		}
	}
	pointNormalize.clear();
	for (int x = 0; x < img.cols; x++) {
		for (int y = 0; y < img.rows; y++) {
			Scalar color = img.at<Vec3b>(Point(x, y));
			if (color[0] == 255 && color[1] == 255 && color[2] == 255) {
				//build stack
				pointNormalize.push_back(Point(x, y));
			}
			else if (!pointNormalize.empty() && color[0] == 0 && color[1] == 0 && color[2] == 0 && pointNormalize.size() != 1) {
				auto mid = pointNormalize.begin() + pointNormalize.size() / 2;
				Point middle = *mid;
				Vec3b color(255, 255, 255);
				normalizeImg.at<Vec3b>(middle) = color;
				pointNormalize.clear();
			}
		}
	}
	pointNormalize.clear();
}