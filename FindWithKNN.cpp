#include <iostream>
#include <math.h> 
#include <time.h> 
#include "opencv2/core.hpp"
#include <opencv2/opencv.hpp>
using namespace std;
using namespace cv;
Mat img, normalizeImg;
void generatePoint();
int main() {
	vector<Point> pointStack, excludePoint,naviPoint,finalPoint;
	img = Mat::zeros(Size(360, 360), CV_8UC3);
	
	generatePoint();
	//step1
	for (int y = 0; y < img.rows; y++) {
		for (int x = 0; x < img.cols; x++) {
			Scalar color = img.at<Vec3b>(Point(x, y));
			if (color[0] == 255 && color[1] == 255 && color[2] == 255) {
				//build stack
				pointStack.push_back(Point(x, y));
			}
		}
	}
	cout <<"Intial" << endl <<pointStack << endl;
	float minTotalMag = numeric_limits<float>::max();
	for (int i = 0; i < pointStack.size(); i++) {
		float totalMag = 0;
		Point startPoint = pointStack.at(i);
		excludePoint.push_back(startPoint);
		naviPoint.push_back(startPoint);
		for (int j = 0; j < pointStack.size(); j++) {
			Point nextPoint = naviPoint.back();
			Point tempPoint;
			float minMag = numeric_limits<float>::max();
			if (excludePoint.size() == pointStack.size()) {
				break;
			}
			for (int k = 0; k < pointStack.size(); k++) {
				//choose line
				Point selectedPoint = pointStack.at(k);
				if (find(excludePoint.begin(), excludePoint.end(), selectedPoint) == excludePoint.end()) {
					//if element is not found
					float magnitude = sqrtf((selectedPoint.x - nextPoint.x) * (selectedPoint.x - nextPoint.x) + (selectedPoint.y - nextPoint.y) * (selectedPoint.y - nextPoint.y));
					if (magnitude < minMag) {
						minMag = magnitude;
						tempPoint = selectedPoint;
					}
					//cout <<"Magnitude: "<< selectedPoint <<"\t"<< magnitude << endl <<endl;
				}
				else
					continue;
			}
			//select this line
			nextPoint = tempPoint;
			excludePoint.push_back(nextPoint);
			naviPoint.push_back(nextPoint);
			totalMag += minMag;
			//cout << naviPoint << endl << endl;
		}
		//start point
		totalMag += sqrtf((naviPoint.back().x - startPoint.x) * (naviPoint.back().x - startPoint.x) + (naviPoint.back().y - startPoint.y) * (naviPoint.back().y - startPoint.y));
		naviPoint.push_back(startPoint);
		//cout << naviPoint<< "\t"<<totalMag<< endl << endl;
		
		if (totalMag < minTotalMag) {
			minTotalMag = totalMag;
			finalPoint = naviPoint;
		}
		naviPoint.clear();
		excludePoint.clear();
	}
	//cout<< "Final " << endl << finalPoint << endl;
	for (int i = 1; i < finalPoint.size(); i++) {
		try {
			circle(img, finalPoint.front(), 3, Scalar(255, 0), 2);
			//arrowedLine(img, finalPoint.at(i), finalPoint.at(i + 1), Scalar(0, 255), 1);
			line(img, finalPoint.at(i-1), finalPoint.at(i), Scalar(0, 255), 1);
		}
		catch (out_of_range &e) {
			break;
		}
	}
	imshow("img", img);
	waitKey(0);
	return 0;
}
void generatePoint() {
	srand(time(NULL));
	Point center1(180, 180);
	int radius = 100;
	for (int i = 0; i < 100; i++) {
		int random = rand() % 360;
		//int randomY = rand() % 360;
		int angle = random;
		int x = center1.x + cos(angle) *radius;
		int y = center1.y + sin(angle) *radius;
		Point plotPoint(x, y);
		Vec3b color(255, 255, 255);
		img.at<Vec3b>(plotPoint) = color;
	}
}